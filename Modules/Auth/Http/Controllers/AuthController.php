<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Auth\Entities\User;

class AuthController extends Controller
{

    /**
     * Show the form for creating a new user resource.
     * @return Renderable
     */
    public function createUser()
    {
        return view('auth::register');
    }

    /**
     * Store a newly created user resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function storeUser(Request $request)
    {
        $attributes = request()->validate([
            'name' => 'required|max:255',
            'username' => 'required|max:255|unique:users,username',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|min:7|max:255',
        ]);

        // $attributes['password'] = bcrypt($attributes['password']);

        $user = User::create($attributes);

        // session()->flash('success','Your account has been created');

        // return redirect('/');

        auth()->login($user);

        return redirect('/home')->with('success','Your account has been created');
    }

    /**
     * Show the form for creating a new session(login).
     * @return Renderable
     */
    public function createSession()
    {
        return view('auth::login');
    }

    /**
     * Store a newly created session in storage.
     * @param Request $request
     * @return Renderable
     */
    public function storeSession(Request $request)
    {
        // validate the request
        $attributes = request()->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        
        if (! auth()->attempt($attributes)) {
            throw ValidationException::withMessages([
                'email' => 'Your provided credentials could not be verified.'
            ]);
        }

        //auth failed
        // return back()
        //         ->withInput()
        //         ->withErrors(['email' => 'Your provided credentials could not be verified.']);
        session()->regenerate();
            
        return redirect('/home')->with('success','Welcome Back!');
    }

    /**
     * destroy session in storage.
     * @param Request $request
     */
    public function destroy()
    {
        auth()->logout();

        return redirect('/')->with('success', 'Goodbye!');
    }

    
}
