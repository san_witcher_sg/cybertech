<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('register_view');
});

Route::get('register', 'AuthController@createUser')->name('register_view')->middleware('guest');
Route::post('register', 'AuthController@storeUser')->name('register')->middleware('guest');

Route::get('login', 'AuthController@createSession')->name('login_view')->middleware('guest');
Route::post('login', 'AuthController@storeSession')->name('login')->middleware('guest');
Route::post('logout', 'AuthController@destroySession')->name('logout')->middleware('auth');
