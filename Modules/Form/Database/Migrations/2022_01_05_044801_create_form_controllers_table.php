<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormControllersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('form_fields', function (Blueprint $table) {
            $table->id();
            $table->foreignId('form_id');
            $table->string('label');
            $table->foreignId('form_controller_id');
            $table->string('required')->default(0);
            $table->timestamps();
        });

        Schema::create('form_inputs', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->timestamps();
        });

        Schema::create('multiple_values', function (Blueprint $table) {
            $table->id();
            $table->foreignId('form_field_id');
            $table->string('label');
            $table->string('value');
            $table->boolean('selected')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
        Schema::dropIfExists('form_fields');
        Schema::dropIfExists('form_controllers');
        Schema::dropIfExists('multiple_values');
    }
}
