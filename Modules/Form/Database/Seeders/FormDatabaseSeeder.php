<?php

namespace Modules\Form\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Form\Entities\FormInput;

class FormDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        FormInput::truncate();

        FormInput::create([
            'type' => 'header'
        ]);

        FormInput::create([
            'type' => 'text'
        ]);

        FormInput::create([
            'type' => 'textarea'
        ]);

        FormInput::create([
            'type' => 'radio-group'
        ]);

        FormInput::create([
            'type' => 'select'
        ]);

        FormInput::create([
            'type' => 'date'
        ]);
    }
}
