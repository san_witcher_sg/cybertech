<?php

namespace Modules\Form\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Form extends Model
{
    use HasFactory;

    /**
     * The attributes that are guraded.
     *
     * @var string[]
     */
    protected $guarded = [];
    
    protected static function newFactory()
    {
        return \Modules\Form\Database\factories\FormFactory::new();
    }

    /**
     * A Form may have many Form Fields
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formFields()
    {
        return $this->hasMany('App\Modules\Form\Entities\FormField');
    }
}
