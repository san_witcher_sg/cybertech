<?php

namespace Modules\Form\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FormField extends Model
{
    use HasFactory;

    /**
     * The attributes that are guraded.
     *
     * @var string[]
     */
    protected $guarded = [];
    
    protected static function newFactory()
    {
        return \Modules\Form\Database\factories\FormFieldFactory::new();
    }
    
    /**
     * A Form Field should have a Form
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function form()
    {
        return $this->belongsTo('App\Modules\Form\Entities\Form');
    }

    /**
     * A Form Field should have a Form Input
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formInput()
    {
        return $this->belongsTo('App\Modules\Form\Entities\FormField');
    }
}
