<?php

namespace Modules\Form\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class FormController extends Controller
{
    /**
     * Display the relevant dashboard.
     * @return Renderable
     */
    public function dashBoard()
    {
        $user_role = Auth::user()->role;

        if($user_role == 1){
            return view('form::admin.create');
        }else{
            return view('form::user.store');
        }
    }

    /**
     * Store a newly created form in storage.
     * @param Request $request
     * @return json response
     */
    public function form_create(Request $request)
    {
        // $validator = Validator::make($request->fields, [
        //     // 'field.*.label' => 'required',
        //     // 'field.*.type' => 'required',
        //     // 'field.*.required' => 'required',
        //     // 'field.*.values.*.label' => 'required_if:field.type,radio-group,select',
        //     // 'field.*.values.*.value' => 'required_if:field.type,radio-group,select',
        //     // 'field.*.values.*.selected' => 'required_if:field.type,radio-group,select',
        // ]);

        for ($i=1; $i <= count($request->fields); $i++) {
            
        }

        // if ($validator->passes()) {

        //     // Store Data in DATABASE from HERE
            


        //     return response()->json(['success'=>'Added new records.']);
            
        // }

        // return response()->json(['error'=>$validator->errors()]);
        return response()->json(count($request->fields));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('form::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('form::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
