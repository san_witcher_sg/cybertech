@extends('form::layouts.app')

@section('navigation')

    <li class="nav-item">
        <a href="#" class="nav-link active">
            <i class="far fa-circle nav-icon"></i>
            <p>Create Form</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>All Forms</p>
        </a>
    </li>

@endsection

@section('content')

    <h1>Custom Form Builder</h1>
    
    <form>
        @csrf
    </form>
    
    <div class="alert alert-success alert-block" style="display: none;">
        <button type="button" class="close" data-dismiss="alert">×</button>
          <strong class="success-msg"></strong>
    </div>

    <div id="build-wrap"></div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://formbuilder.online/assets/js/form-builder.min.js"></script>
    <script>
        
        jQuery(function($) {
            
            var fbEditor = document.getElementById('build-wrap');
            
            var options = {
                disableFields: [
                    'autocomplete',
                    'button',
                    'file',
                    'checkbox-group',
                    'hidden-input',
                    'number',
                    'hidden',
                    'paragraph'
                ],
                disabledActionButtons: ['data','save'],
                actionButtons: [{
                    id: 'save_btn',
                    className: 'btn btn-success',
                    label: 'Save',
                    type: 'button',
                    events: {
                        click: function() {
                            // alert(formBuilder.actions.getData());
                            var _token = $("input[name='_token']").val();

                            $.ajax({
                                url: "{{ route('form_create') }}",
                                type:'POST',
                                data: {_token:_token, fields:formBuilder.actions.getData()},
                                success: function(data) {
                                    // if($.isEmptyObject(data.error)){
                                    //     console.log(data.success);
                                    //     $('.alert-block').css('display','block').append('<strong>'+data.success+'</strong>');
                                    // }else{
                                    //     $.each( data.error, function( key, value ) {
                                    //     $('.'+key+'_err').text(value);
                                    //     });
                                    // }
                                    // alert(data);
                                    console.log(data);
                                }
                            });
                        }
                    }
                }]
            };

            var formBuilder = $(fbEditor).formBuilder(options);

            document.getElementById('showData').addEventListener('click', function() {
                formBuilder.actions.showData()
            });

        });

    </script>

@endsection
