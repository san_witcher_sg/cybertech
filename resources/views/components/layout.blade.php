<!doctype html>

<title>Laravel From Scratch Blog</title>
<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
<script src="//unpkg.com/alpinejs" defer></script>

<body style="font-family: Open Sans, sans-serif">
    <section class="px-6 py-8">
        <nav class="md:flex md:justify-between md:items-center">
            <div>
    
            </div>

            <div class="mt-8 md:mt-0 flex items-center">
               
                @auth
                    {{-- <a href="/logout" class="text-xs font-bold uppercase">Logout</a> --}}
                    <span class="text-xs font-bold uppercase">Welcome, {{ auth()->user()->name }}!</span>

                    <form action="/logout" method="POST" class="text-xs font-semibold text-blue-500 ml-6">
                        
                        @csrf

                        <button type="submit">Logout</button>
                    
                    </form>
                
                @else
                    
                    <a href="/register" class="text-xs font-bold uppercase">Register</a>

                    <a href="/login" class="ml-6 text-xs font-bold uppercase">Log In</a>

                @endauth
            
            </div>
        
        </nav>

        {{ $slot }}

    </section>

    @if (session()->has('success'))
        
        <div x-data="{ show: true }" 
                x-init="setTimeout(() => show = flase, 4000)" 
                x-show="show" 
                class="fixed bg-blue-500 text-white py-2 px-4 rounded-xl bottom-3 right-3 text-sm">
            
            <p>{{ session('success') }}</p>
        
        </div>

    @endif

</body>
